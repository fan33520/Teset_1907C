## 2019.06.21

| 人员 | 今日任务 | 遇到困难 | 笔记整理 | 录音整理 |
| ---- | -------- | -------- | -------- | -------- |
|   侯岳   |     Python文件操作及面向对象及函数练习题	      |     没有困难     |     https://houyue123.gitee.io/bob-blog     |          |
|   李逸宁   |    Python文件操作及面向对象及函数练习题       |     没有困难     |    https://www.cnblogs.com/Stubbornlyn/     |          |
|   李仕超   |       Python文件操作及面向对象及函数练习题     |     没有困难     |    https://l_s_chao.gitee.io/hugo/   |          |
|   高登   |      Python练习      |     没有困难     |    https://www.cnblogs.com/fairytalk     |          |
|   王靖媛   |     Python练习      |     没有困难     |     https://my.csdn.net/weixin_47247194     |          |
|   曹海龙   |      Python练习       |     没有困难     |     https://sseechl.gitee.io/bg/     |          |
|   王浩然   |     Jmeter接口测试和参数关联     |     没有困难     |     https://blog.csdn.net/weixin_44993102     |          |