## 2020.06.20

| 人员 | 今日任务 | 遇到困难 | 笔记整理 | 录音整理 |
| ---- | -------- | -------- | -------- | -------- |
|秦仕炜 |python练习题| 没有困难   | https://www.cnblogs.com/qin-shi-wei/     |          |
|张誉馨 |  python练习题| 没有困难   | https://blog.csdn.net/weixin_45321789        |          |
|康高祥|  python练习题| 没有困难 | https://kan_gao_xiang.gitee.io/hugoblog         |          |
|贾冬冬|  python练习题| 没有困难|https://blog.csdn.net/weixin_43675386          |          |
|陈士方|  python练习题|没有困难|http://zfblog.xyz          |          |
|杨亚涛 | python练习题|   没有困难     |   https://blog.csdn.net/Sk_Ya         |          |

