## 2020.06.21

| 人员 | 今日任务 | 遇到困难 | 笔记整理 | 录音整理 |
| ---- | -------- | -------- | -------- | -------- |
|   李雪燕   |    Python中面向对象及函数      |     没有困难     |     https://juejin.im/user/5ecfa133e51d4578702f4c10/posts  |          |
|   张艺舒   |    Python中面向对象及函数      |     没有困难     |     https://blog.csdn.net/weixin_43805807    |          |
|   王成伍   |    Python中面向对象及函数      |     没有困难     |     https://blog.csdn.net/weixin_47111376     |          |
|   窦本致   |    Python中面向对象及函数      |     没有困难     |     https://blog.csdn.net/douxiaozhi     |          |
|   宋国栋   |    Python中面向对象及函数      |     没有困难     |     https://sgd1175431623.gitee.io/my_hexo/     |          |
|   单兴伟   |    Python中面向对象及函数      |     没有困难     |     https://blog.csdn.net/Mr_ShanXingWei     |          |

