## 2020.06.21

| 人员   | 今日任务 | 遇到困难 | 笔记整理                      | 录音整理 |
| ------ | -------- | -------- | ----------------------------- | -------- |
| 李超前 | python文件操作以及捕获异常 | 无 | https://huo_hai.gitee.io/lcq/ |          |
| 刘星伸 | python文件操作以及捕获异常 | 无 | https://tianshangxing.github.io/blog/ |          |
| 赵成越 | python文件操作以及捕获异常 | 无 | https://blog.csdn.net/weixin_45791844 |          |
| 于强强 | python文件操作以及捕获异常 | 无 | https://www.cnblogs.com/yuqaingblog/ |          |
| 梁景琴 | python文件操作以及捕获异常 | 无 | https://blog.csdn.net/weixin_45820315 |          |
| 付佳康 | python文件操作以及捕获异常 | 无 | http://fu_jia_kang.gitee.io/my_technology/ |          |

