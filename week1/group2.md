## 2019.06.21

| 人员 | 今日任务 | 遇到困难 | 笔记整理 | 录音整理 |
| ---- | -------- | -------- | -------- | -------- |
|  张鹏   |  Python异常捕获和文件操作  |  没有困难  |https://www.jianshu.com/p/c12b7f6f99fe                   |          |
|  邢壮壮 |  Python异常捕获和文件操作  |  没有困难  |https://xing_zhuang_zhuang.gitee.io/my_technology_blog   |          |
|  王晋伟 |  Python异常捕获和文件操作  |  没有困难  |https://mp.csdn.net/console/home                         |          |
|  兰宁   |  Python异常捕获和文件操作  |  没有困难  |https://lan-warmth.github.io/wenqing.github.io/          |          |
|  马瑞春 |  Python异常捕获和文件操作  |  没有困难  | https://a_can_of_words.gitee.io/mrc_blog                |          |
|  陈威   |  Python异常捕获和文件操作  |  没有困难  |https://www.jianshu.com/u/40e45cfa5aab                   |          |
